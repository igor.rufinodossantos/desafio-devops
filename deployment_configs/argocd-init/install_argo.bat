@echo off

echo Renomeando os arquivos na pasta argocd-init...
echo.

rename .\helm helm.exe
if ERRORLEVEL 1 exit /b %ERRORLEVEL%

rename .\istioctl istioctl.exe
if ERRORLEVEL 1 exit /b %ERRORLEVEL%

rename .\kubectl kubectl.exe
if ERRORLEVEL 1 exit /b %ERRORLEVEL%
echo.

echo Adicionando o repositorio do chart do Argo CD...
call .\helm.exe repo add argo https://argoproj.github.io/argo-helm
if ERRORLEVEL 1 exit /b %ERRORLEVEL%
echo.

echo Atualizando a lista de charts disponíveis...
call .\helm.exe repo update
if ERRORLEVEL 1 exit /b %ERRORLEVEL%
echo.

echo Instalando o chart do Argo CD...
call .\helm.exe install argocd argo/argo-cd --version 6.7.9 --values values.yaml --namespace argocd --create-namespace
if ERRORLEVEL 1 exit /b %ERRORLEVEL%
echo.

echo Pausando o script por 20 segundos...
timeout /t 20
if ERRORLEVEL 1 exit /b %ERRORLEVEL%
echo.

echo Configurando o Istio...
echo Adicionando o comando istioctl ao PATH...
set PATH=%PATH%;%CD%\argocd-init
if ERRORLEVEL 1 exit /b %ERRORLEVEL%
echo.

echo Instalando o Istio default...
call .\istioctl.exe install --set profile=default -y
if ERRORLEVEL 1 exit /b %ERRORLEVEL%
echo.

echo Pausando o script por 10 segundos...
timeout /t 10
if ERRORLEVEL 1 exit /b %ERRORLEVEL%
echo.


echo Instalando o cert...
call .\istioctl.exe install --set profile=default -y
if ERRORLEVEL 1 exit /b %ERRORLEVEL%
echo.


echo Aplicando a configuração de Application do Argo CD...
call .\kubectl.exe apply -f argo-init.yaml
if ERRORLEVEL 1 exit /b %ERRORLEVEL%
echo.

echo Renomeando os arquivos na pasta argocd-init...
echo.

rename .\helm.exe helm
if ERRORLEVEL 1 exit /b %ERRORLEVEL%

rename .\istioctl.exe istioctl
if ERRORLEVEL 1 exit /b %ERRORLEVEL%

rename .\kubectl.exe kubectl
if ERRORLEVEL 1 exit /b %ERRORLEVEL%
echo.

echo Script concluído com sucesso!
