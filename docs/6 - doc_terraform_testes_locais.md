## Pré-requisitos Detalhados

### Terraform

#### Instalação do Terraform

1. **Download e Instalação**: 
   - Acesse a [página de downloads do Terraform](https://www.terraform.io/downloads.html) e baixe a versão adequada para o SO.

2. **Verificação da Instalação**: 
   - Abra um terminal e execute `terraform version` para confirmar a instalação. A saída deve indicar a versão instalada, que deve ser `0.13.0` ou superior.

### Google Cloud Platform (GCP)

#### Criação de Projeto e Configuração de Acesso

1. **Criação de um Projeto no GCP**:
   - No [Console do GCP](https://console.cloud.google.com/), crie um novo projeto, especificando um nome e, se aplicável, uma organização.

2. **Conta de Serviço e Credenciais**:
   - Crie uma conta de serviço dentro do projeto com o papel de "Editor" para obter permissões necessárias para gerenciar recursos.
   - Gere e baixe um arquivo `credentials.json` com as chaves de acesso.

3. **Bucket do GCS para o Backend do Terraform**:
   - Crie um bucket no Google Cloud Storage para armazenar o estado do Terraform(Backend).

## Configuração do Terraform e Testes Locais


### Inicialização e Validação

1. **Inicialização do Terraform**:
   - Dentro de `environments/dev`, execute `terraform init` para preparar o diretório, inicializando o backend e baixando os providers.

2. **Formatação e Validação**:
   - Use `terraform fmt` para formatar os arquivos de acordo com as convenções de estilo do Terraform.
   - Execute `terraform validate` para verificar erros na configuração.

### Planejamento e Aplicação

1. **Planejamento**:
   - `terraform plan` gera um plano de execução, mostrando quais recursos serão criados, modificados ou destruídos. Este passo é fundamental para revisar as mudanças antes de aplicá-las.

2. **Aplicação**:
   - Com `terraform apply`, as alterações especificadas no plano são aplicadas no GCP. É solicitada uma confirmação antes da execução, garantindo que o usuário revise as alterações.

### Verificação e Destruíção

1. **Verificação**:
   - Após a aplicação, use o Console do GCP para verificar se os recursos foram criados conforme esperado. Os `outputs` do Terraform podem fornecer informações úteis, como IPs e nomes de recursos.

2. **Limpeza**:
   - Execute `terraform destroy` para remover todos os recursos criados, evitando custos desnecessários e mantendo o ambiente limpo.

## Conclusão

Este guia detalhado fornece as instruções passo a passo, desde a configuração inicial até a aplicação e verificação de uma infraestrutura de VPC e cluster no GCP usando Terraform. Contemplando a instalação do Terraform, a configuração do projeto GCP, e o processo de teste local.