# Documentação da Stack de Monitoramento

## Visão Geral

Este documento descreve a configuração da  stack de monitoramento, implementada no Kubernetes através do ArgoCD, utilizando as ferramentas Prometheus e Grafana. A stack é configurada para monitorar diversos aspectos do cluster, incluindo métricas de sistemas, aplicações e infraestrutura.

## Configurações Principais

### CRDs do Prometheus

- **Arquivo de Configuração**: `prometheus-crds`
- **Versão**: Utilizamos a versão `5.0.0` do chart `prometheus-operator-crds` do repositório Helm `prometheus-community`.
- **Namespace**: `monitoring`
- **Política de Sincronização**: A configuração é automaticamente aplicada, com a capacidade de auto-cura e poda habilitadas.

Este conjunto inicial de Custom Resource Definitions (CRDs) do Prometheus prepara o cluster para o restante da stack, garantindo que os recursos necessários estejam definidos e prontos para uso.

### Prometheus e Grafana (Kube-Prometheus-Stack)

- **Arquivo de Configuração**: `prometheus`
- **Versão**: Versão `48.1.1` do chart `kube-prometheus-stack`.
- **Namespace**: `monitoring`
- **Configurações Importantes**:
  - **Node Exporter**: Habilitado para coleta de métricas do sistema operacional Linux.
  - **Grafana**: Configurado com URL específica (`https://grafana.desafio.com.br`) e senha de administração. Persistência habilitada para armazenamento de dashboards e dados.
  - **Prometheus Operator**: Habilitado para gerenciar a instância do Prometheus e suas configurações.
  - **Scrape Configs Adicionais**: Configurações para scraping de métricas do Pushgateway, Istiod, e métricas de pods com base em padrões de nomeação específicos.

Esta configuração cria uma instância completa do Prometheus para scraping e armazenamento de métricas, juntamente com o Grafana para visualização e análise desses dados. A stack é projetada para ser auto-gerenciável e resiliente, com políticas de auto-cura e poda.

## Acesso aos Dashboards

Os dashboards do Grafana são acessíveis através da URL configurada (`https://grafana.desafio.com.br`), utilizando a senha de administração definida nas configurações do Helm. Esses dashboards permitem uma visualização detalhada das métricas coletadas pelo Prometheus, oferecendo insights valiosos sobre o estado do cluster e das aplicações.

### Configurações de Dashboard

Os dashboards são configurados para serem dinamicamente descobertos e carregados através do sidecar do Grafana, com suporte para atualizações via UI e uma estrutura de pastas baseada na configuração dos arquivos. Isso facilita a gestão e atualização dos dashboards, permitindo uma personalização profunda para atender às necessidades de monitoramento específicas.

## Conclusão

A stack de monitoramento implementada proporciona uma visão abrangente e detalhada do estado do cluster Kubernetes, utilizando  coletas, armazenamento e visualização de métricas.