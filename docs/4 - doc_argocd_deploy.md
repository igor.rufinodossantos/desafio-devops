# Documentação ArgoCD - Visão Geral e Configurações

## O que é o ArgoCD?

ArgoCD é uma ferramenta de entrega contínua baseada no GitOps para Kubernetes, permitindo que desenvolvedores e operadores usem o Git como a fonte única da verdade para definição e estado desejado de suas aplicações. Automatizando a implantação de aplicações conforme as configurações definidas em um repositório Git, o ArgoCD facilita a gestão de infraestrutura e aplicações de forma declarativa.

### Principais Características:

- **Declarativo**: Gerencia aplicações definidas por arquivos declarativos armazenados no Git.
- **Automatizado**: Atualiza automaticamente o estado dos ambientes para corresponder ao estado definido no Git.
- **Autônomo**: Mantém as aplicações sincronizadas com as especificações sem intervenção manual, com opções para sincronização automática ou manual.

### Conceitos Específicos:

- **Application**: Um conjunto de recursos Kubernetes definidos por um manifesto, representando uma aplicação completa.
- **Target state**: O estado desejado da aplicação, representado por arquivos em um repositório Git.
- **Live state**: O estado atual da aplicação no cluster.
- **Sync status**: Indica se o estado atual corresponde ao estado desejado.

## Instalação do ArgoCD

A instalação do ArgoCD atual vai variar dependendo do sistema operacional, mas geralmente segue uma abordagem padrão adaptável a diferentes ambientes. A seguir, uma visão simplificada do processo de instalação em um ambiente Windows, com adaptações para outros sistemas operacionais quando necessário.

### Estrutura de Diretório para Configuração:

dentro do repositório `desafio-devops`, na pasta `deployment_config`, os seguintes itens são pré-configurados para facilitar a instalação do ArgoCD, especialmente em ambientes Windows:

```

desafio-devops/
└── deployment_config/
    ├── apis           # Contém as configurações específicas para as APIs da aplicação, incluindo manifestos do Kubernetes.
    ├── argocd         # Armazena manifestos e configurações necessárias para a instalação e configuração do ArgoCD.
    ├── argocd-init    # Inclui scripts e arquivos de inicialização para o ArgoCD, como `install_argo.bat` e `argo-init.yaml`.
    ├── helm           # Contém os binários do Helm, ferramenta de gerenciamento de pacotes do Kubernetes, facilitando a instalação de charts.
    ├── istio          # Guarda os binários do Istio e configurações relacionadas, utilizados para configurar o service mesh no cluster Kubernetes.
    └── monitoring     # Reúne ferramentas e configurações para o monitoramento do cluster, incluindo Prometheus, Grafana, e outros.


```

### Instalação do ArgoCD

```
desafio-devops/
└── deployment_config/
    ├── argocd-init
        ├── argo-init.yaml
        ├── helm (binário)
        ├── install_argo.bat
        ├── istioctl (binário)
        └── kubectl (binário)
```

### Benefícios da Inclusão de Binários:

1. **Facilidade de Instalação**: Ao incluir binários como `helm`, `istioctl`, e `kubectl` diretamente no repositório, eliminamos a necessidade do usuário final instalar essas ferramentas separadamente. Isso é particularmente útil em ambientes restritos ou quando se deseja garantir uma experiência de instalação sem esforço para o cliente.

2. **Consistência e Controle de Versão**: Garante que todos os usuários estejam utilizando as mesmas versões das ferramentas necessárias para a instalação e configuração do ArgoCD, reduzindo problemas de compatibilidade e simplificando o suporte técnico.

3. **Automação Simplificada**: O script `install_argo.bat` utiliza esses binários para executar a instalação e configuração do ArgoCD e do Istio, assim como para inicializar o repositório dentro do ArgoCD. Isso torna o processo de instalação praticamente automatizado, exigindo mínima intervenção manual.

Entendido, vamos detalhar mais esses pontos para garantir que o manual de instalação do ArgoCD seja um guia passo a passo completo e detalhado, facilitando a execução por outra pessoa.

## Após Instalação do ArgoCD

Após a instalação do ArgoCD utilizando os scripts e binários fornecidos, siga os passos abaixo para configurar e começar a utilizar o ArgoCD para gerenciamento de suas aplicações Kubernetes.

#### Sincronização de Aplicações

1. **Acesso à UI do ArgoCD**: Para visualizar e gerenciar as aplicações através da interface do usuário (UI) do ArgoCD, é necessário acessar a UI. Isso pode ser feito através do port-forwarding, permitindo o acesso local à interface.

2. **Verificação do Estado de Sincronização**: Na UI do ArgoCD, confira se as aplicações estão sincronizadas com os estados definidos nos repositórios Git. As aplicações devem refletir o estado desejado conforme a configuração declarativa no Git.

#### Configuração de Hosts e Acesso

1. **Configuração do Arquivo de Hosts**: Para facilitar o acesso aos serviços gerenciados pelo ArgoCD e Istio utilizando nomes de domínio amigáveis, como `argocd.desafio.com.br`, edite o arquivo de hosts do seu sistema operacional. Adicione uma entrada para cada serviço, apontando para o IP externo do Istio Ingress Gateway. Esse IP pode ser encontrado no serviço dentro do namespace `istio-system`.

    Exemplo de entrada no arquivo de hosts:
    ```
    <ip_externo_istio> argocd.desafio.com.br
    <ip_externo_istio> grafana.desafio.com.br
    ```

2. **Acesso via Port-Forwarding**: Para acessar a UI do ArgoCD antes de configurar o DNS ou em ambientes de teste, utilize o kubectl para fazer port-forwarding do serviço argocd-server para a sua máquina local:
    ```sh
    kubectl port-forward svc/argocd-server -n argocd 8080:443
    ```
    A UI do ArgoCD estará acessível em `localhost:8080`.

#### Recuperando a Senha do Admin Inicial

1. **Acesso às Credenciais Iniciais**: Após a instalação, a senha inicial para o usuário admin do ArgoCD é armazenada em uma secret no Kubernetes chamada `argocd-initial-admin-secret`, localizada no namespace do ArgoCD. Para recuperá-la, execute:
    ```sh
    kubectl get secret argocd-initial-admin-secret -n argocd -o jsonpath="{.data.password}" | base64 --decode; echo
    ```
    Utilize essas credenciais para fazer o primeiro login na UI do ArgoCD.


# Conclusão

O ArgoCD representa uma evolução na automação e gerenciamento de infraestrutura Kubernetes.