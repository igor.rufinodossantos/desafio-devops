#  Deploy Dockerfile e Testes Locais

## Construção da Imagem Docker

Para construir uma imagem Docker da aplicação usando o `Dockerfile`. Acesse o  o diretório que contém o `Dockerfile` e execute o seguinte comando:

```bash
docker build -t api-comentarios .
```

Este comando constrói uma nova imagem Docker com o nome `api-comentarios` baseado nas instruções definidas no `Dockerfile`. O ponto `.` ao final indica o contexto de construção, geralmente o diretório atual onde o `Dockerfile` está localizado.

## Executando o Container em Modo Interativo

Para realização de teste, podemos  iniciar o container em modo interativo com acesso ao terminal. Utilizado para verificar rapidamente a funcionalidade:

```bash
docker run -it --rm -p 8000:8000 api-comentarios
```

Neste comando:
- `-it` permite interação interativa com o container.
- `--rm` remove automaticamente o container quando ele é parado. Isso é útil para testes, pois evita acumular containers não utilizados.
- `-p 8000:8000` mapeia a porta 8000 do container para a porta 8000 do host, permitindo que acesse a API localmente.

## Executando o Container em Segundo Plano

Para executar o container em segundo plano, use o seguinte comando:

```bash
docker run -d -p 8000:8000 api-comentarios
```

Aqui, `-d` inicia o container em modo detached. Podendo então acessar a aplicação como se estivesse rodando localmente, usando `localhost:8000` ou o IP do seu Docker host seguido pela porta mapeada.

## Acessando o Container com Docker Exec
Com o container rodando (especialmente em modo detached), podemos executar comandos dentro do container ou acessar seu shell, usando `docker exec` para isso. Primeiro, precisamos do ID ou nome do seu container com `docker ps`, e então:

```bash
docker exec -it nome_ou_id_do_container /bin/bash
```

Isso abrirá um shell Bash dentro do container.

## Verificando Logs do Container

Para verificar os logs de um container em execução,  use:

```bash
docker logs nome_ou_id_do_container
```

Este comando exibirá os logs gerados pelo Gunicorn e sua aplicação Flask, permitindo uma rápida depuração e acompanhamento das requisições.

## Conclusão

Combinando todos esses passos, há um fluxo completo desde a construção da imagem Dockerda aplicação até a execução e testes locais, seja interativamente ou em segundo plano.