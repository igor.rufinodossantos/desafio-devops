# Atenção

Para proporcionar uma transição suave e garantir que você tenha todas as informações necessárias ao mover ou configurar seu repositório em um novo ambiente, é essencial detalhar cada um dos pontos mencionados. Aqui está uma visão abrangente, considerando cada aspecto crucial para a configuração e o funcionamento adequados do pipeline CI/CD, Terraform e ArgoCD.

## Pré-Requisitos e Pontos de Atenção Completos

### Acesso Necessário
1. **Acesso à GCP (Google Cloud Platform)**: Para criar e gerenciar recursos, como clusters Kubernetes. É necessário configurar permissões adequadas na console da GCP.
2. **Acesso ao GitLab**: Para gerenciar o projeto, configurar variáveis de CI/CD e ajustar as definições do pipeline.

### Credentials e Configurações
3. **Credentials da GCP**: Usadas para autenticar no GCP ao criar o cluster Kubernetes e executar outras operações. Devem ser configuradas como parte do projeto no GitLab CI/CD e no Terraform.
4. **Credentials para executar o Terraform**: Necessárias para autenticar no provedor GCP ao aplicar configurações de infraestrutura.
5. **Credentials para acessar o Storage GCP**: Importantes para o Terraform armazenar o estado da infraestrutura no Google Cloud Storage.
6. **Storage GCP criado**: Essencial ter um bucket configurado no GCS para ser usado como backend do Terraform.
7. **Alterações no Backend do Terraform**: Ajuste as configurações do backend no arquivo `backend.tf` para apontar para o storage do GCS recém-criado.

### Ajustes no Repositório e Infraestrutura
8. **URL do Repositório e Revisão no ArgoCD**: Atualize o `repoUrl` e `targetRevision` nos manifestos do ArgoCD para refletir a estrutura do novo repositório e a branch de interesse.
9. **Alteração do repoUrl e targetRevision no argo-init**: Similar ao ponto anterior, assegure que o script ou manifest de inicialização do ArgoCD (`argo-init.yaml`) reflita o novo repositório Git e a revisão alvo.
10. **Binários conforme o SO**: Caso esteja migrando para ou utilizando um sistema operacional diferente de Windows, é necessário substituir os binários (`helm`, `istioctl`, `kubectl`) pelos correspondentes ao seu sistema operacional atual.
11. **Configurações do `values.yaml` para o ArgoCD**: No arquivo `values.yaml` que configura o ArgoCD, atualize as informações de `url`, `username`, e `password` para garantir que o ArgoCD possa acessar o repositório de configuração de maneira autenticada.
12. **Atualização do `regcred.yaml`**: No arquivo `deployment_configs/default/regcred.yaml`, atualize as credenciais do registro Docker (denominadas `regcred`) para permitir que o Kubernetes puxe imagens de contêineres privadas.
13. **Ajuste dos Gateway e Virtual Services**: Se necessário, modifique os arquivos de configuração do Gateway e VirtualServices dentro do diretório `istio` para adequar os hosts e rotas de acesso às suas aplicações.
14. **Alteração da senha do Prometheus**: Caso deseje alterar a senha padrão do Prometheus, faça isso dentro do diretório `monitoring` para manter o acesso às métricas seguro.
15. **Modificação no script `update_image.sh`**: No script `update_image.sh`, ajuste o caminho para a aplicação que precisa ter a tag atualizada para refletir corretamente a nova imagem no ArgoCD.

### Configurações do Terraform e GitLab CI
16. **Criação da `credentials.json` no GCP**: Gere uma chave de conta de serviço no GCP com as permissões apropriadas e adicione esse arquivo `credentials.json` ao Terraform e no GitLab CI/CD.
17. **Variáveis do Terraform**: No arquivo `environments/develop/terraform.tfvars`, faça os ajustes necessários para refletir seu ambiente específico, incluindo nomes de projetos, regiões, entre outros.
18. **Token do GitLab (`GITLAB_TOKEN`)**: Crie um token de acesso no GitLab e configure-o como uma variável de ambiente no projeto do GitLab CI para permitir operações de commit e push durante o pipeline.

## Conclusão
Preparar e verificar cada um desses pontos antes de migrar ou configurar seu projeto em um novo ambiente ou repositório.