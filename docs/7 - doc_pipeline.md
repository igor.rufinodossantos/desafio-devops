
## Pipeline CI/CD GitLab

### Definição dos Estágios

```yaml
stages:
  - tag
  - build
  - deploy
```

- `stages`: Define os estágios sequenciais do pipeline. Cada estágio (`tag`, `build`, `deploy`) precisa ser concluído com sucesso antes de prosseguir para o próximo.

### Variáveis Globais

```yaml
variables:
  IMAGE_NAME: registry.gitlab.com/desafio4470013/desafio-devops/comentarios
```

- `variables`: Define variáveis que são acessíveis em todos os estágios do pipeline. `IMAGE_NAME` especifica o local no registro Docker onde a imagem será armazenada.

### Etapa de Tagging

```yaml
tag:
  stage: tag
  before_script:
    - apt-get update && apt-get install -y jq
    - 'if [ -f version_env.sh ]; then source version_env.sh; fi'
```

- `tag`: Nome do job dentro do estágio `tag`.
- `stage: tag`: Especifica que este job pertence ao estágio `tag`.
- `before_script`: Comandos que rodam antes do script principal. Aqui, `jq` é instalado para processamento de JSON, e `version_env.sh` é fonte se existir, garantindo que a tag gerada anteriormente seja usada.

```yaml
script:
  - chmod +x ./scripts/autotag.sh
  - ./scripts/autotag.sh
```

- `script`: Define os comandos que compõem o job de tagging. Torna `autotag.sh` executável e o executa para gerar a nova tag.

```yaml
artifacts:
  paths:
    - version_env.sh
```

- `artifacts`: Especifica os arquivos gerados pelo job que devem ser passados para os próximos estágios. Aqui, `version_env.sh` contém a nova tag.

```yaml
rules:
  - changes:
      - app/**
```

- `rules`: Define condições para a execução do job. Este job só roda se houver mudanças nos arquivos dentro do diretório `app/`.

## Script autotag.sh

```bash
VERSION_FILE="version.json"
```

- Define o caminho para `version.json`, onde a versão atual é armazenada.

```bash
if [ "$CI_COMMIT_REF_NAME" == "develop" ]; then
    VERSION_KEY="alpha"
    NEW_VERSION=$(jq -r ".alpha" $VERSION_FILE | awk -F. -v OFS=. '{$4 = $4 + 1; print}')
elif [ "$CI_COMMIT_REF_NAME" == "main" ]; then
    VERSION_KEY="main"
    NEW_VERSION=$(jq -r ".main" $VERSION_FILE | awk -F. -v OFS=. '{$3 = $3 + 1; $4="0"; print}')
fi
```

- Usa a variável de ambiente `CI_COMMIT_REF_NAME` para determinar a branch e seleciona a chave de versão correspondente. `develop` incrementa a versão `alpha`, enquanto `main` incrementa a versão `main`.
- `jq` lê a versão atual, e `awk` incrementa o número da versão. Para `develop`, aumenta o último segmento. Para `main`, aumenta o penúltimo e zera o último.

```bash
jq ".$VERSION_KEY = \"$NEW_VERSION\"" $VERSION_FILE > temp.$$.json && mv temp.$$.json $VERSION_FILE
```

- Atualiza `version.json` com a nova versão usando `jq` e renomeia o arquivo temporário para `version.json`.

```bash
echo "export NEW_TAG='$NEW_VERSION'" > $CI_PROJECT_DIR/version_env.sh
```

- Exporta a nova tag para `version_env.sh`, permitindo sua reutilização em estágios subsequentes do pipeline.


## Estágio de Build

```yaml
build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
```

- `stage: build`: Especifica que este job pertence ao estágio de `build`.
- `image: docker:latest`: Utiliza a imagem Docker mais recente como ambiente para executar o job. Isso garante que você tenha as últimas funcionalidades e correções de segurança.
- `services: - docker:dind`: Habilita Docker-in-Docker, permitindo construir imagens Docker dentro deste job.

```yaml
script:
  - echo "Building Docker image..."
  - if [ -f version_env.sh ]; then source version_env.sh; fi
  - docker build -t $IMAGE_NAME:$NEW_TAG -f ./app/Dockerfile ./app
  - docker images
  - echo "$CI_REGISTRY_PASSWORD" | docker login registry.gitlab.com -u "$CI_REGISTRY_USER" --password-stdin
  - docker push $IMAGE_NAME:$NEW_TAG
```

- O script inicia ecoando uma mensagem indicando o início do processo de build.
- Verifica se `version_env.sh` existe para utilizar a tag gerada no estágio de `tag`.
- `docker build`: Constrói a imagem Docker usando a `Dockerfile` localizada em `./app` e a tag gerada anteriormente.
- `docker images`: Lista as imagens Docker para verificação.
- `docker login` e `docker push`: Faz login no registro Docker do GitLab e empurra a imagem construída, utilizando credenciais armazenadas nas variáveis de ambiente do CI/CD.

```yaml
artifacts:
  paths:
    - new_tag.txt
```

- Salva `new_tag.txt`, que contém a tag da imagem construída, como um artefato para uso em estágios subsequentes.

```yaml
rules:
  - changes:
      - app/**
```

- Executa este job somente se houver mudanças nos arquivos dentro do diretório `app/`.

## Estágio de Deploy

```yaml
deploy:
  stage: deploy
  before_script:
    - WORKING_DIR=$(pwd)
    - git config --global user.email "gitlab@desafio.com"
    - git config --global user.name "Gitlab CI Desafio"
```

- `stage: deploy`: Define este job como parte do estágio `deploy`.
- `before_script`: Configurações preliminares para o job de deploy. Define o diretório de trabalho atual e configura o usuário do Git para commits.

```yaml
script:
  - echo "Preparando para atualizar a tag da imagem no ArgoCD application manifest..."
  - if [ -f version_env.sh ]; then source version_env.sh; fi
  - git clone --single-branch --branch $CI_COMMIT_BRANCH https://oauth2:$GITLAB_TOKEN@gitlab.com/desafio4470013/desafio-devops.git temp_repo
```

- Prepara-se para atualizar a tag da imagem no manifest do ArgoCD, reutilizando a tag do estágio de `build`.
- Clona o repositório de configuração do ArgoCD numa pasta temporária (`temp_repo`) usando a branch atual (`$CI_COMMIT_BRANCH`) e credenciais de acesso via token.

```yaml
  - cp "$WORKING_DIR/new_tag.txt" temp_repo/new_tag.txt
  - cd temp_repo
  - chmod +x ../scripts/update_image.sh
  - ../scripts/update_image.sh $NEW_TAG
```

- Copia `new_tag.txt` para o repositório temporário e executa o script `update_image.sh` para atualizar a tag da imagem no arquivo de configuração do ArgoCD (`comentarios.yaml`).

```yaml
  - git add deployment_configs/apis/comentarios.yaml
  - if git diff --staged --quiet; then
      echo "Nenhuma alteração detectada em comentarios.yaml. Não há necessidade de commit."
    else
      git commit -m "Atualização da tag da imagem para $NEW_TAG"
      git push --set-upstream https://oauth2:$GITLAB_TOKEN@gitlab.com/desafio4470013/desafio-devops.git $CI_COMMIT_BRANCH
    fi
```

- Adiciona `comentarios.yaml` ao staging do Git e verifica se há mudanças. Se houver, faz um commit com a mensagem de atualização da tag e faz push das mudanças para o repositório remoto, efetivando a atualização no ArgoCD.

```yaml
dependencies:
  - build
```

- Especifica que o job de `deploy` depende do sucesso do job de `build`, garantindo que a imagem já foi construída e empurrada para o registro Docker.

```yaml
rules:
  - changes:
      - app/**
```

- Similar ao estágio de `build`, este job só é executado se houver mudanças no diretório `app/`.