# Documento de Atenção e Práticas Adotadas

## 1. Escolha do GitLab
Decidi experimentar o GitLab para este projeto, apesar de minha experiência prévia com o Bitbucket. O objetivo era me familiarizar e aprender mais sobre essa plataforma, que promete robustas funcionalidades de CI/CD.

## 2. Criação do Token de Acesso no GitLab
Após criar o projeto no GitLab, gerei um token de acesso. Não estava completamente seguro sobre todas as permissões necessárias, então optei por conceder um acesso quase total inicialmente, planejando revisar e restringir essas permissões mais tarde, conforme a necessidade.

## 3. Clone do Projeto e Testes Locais
Realizei o clone do projeto para minha máquina e comecei testando a construção da imagem Docker localmente. Esses testes foram baseados nas documentações que criei: "doc_dockerfile_construção" e "doc_dockerfile_deploy_testes_locais".

## 4. Infraestrutura com Terraform
Admito que não estava completamente à vontade com o Terraform, sentindo falta de maturidade com a ferramenta. Apesar disso, busquei aplicar uma lógica de módulos para facilitar a criação de uma infraestrutura multiambiente, escolhendo o Kubernetes como foco pela sua complexidade e pela necessidade de criar esse tipo de recurso frequentemente.

### 4.1 Criação da VPC
Para a VPC, utilizei módulos do Terraform disponíveis em repositórios conhecidos, buscando guiar-me por documentações e tutoriais online para realizar a configuração corretamente. Detalhes sobre este processo foram descritos nas documentações "doc_terraform_create" e "doc_terraform_teste_locais".

### 4.2 Criação do Cluster Kubernetes
Na tentativa de criar o cluster Kubernetes utilizando módulos, enfrentei dificuldades por falta de experiência, o que me levou a perder bastante tempo em testes. Após uma análise mais profunda, percebi como poderia ter utilizado os módulos eficazmente, optando por uma abordagem mais simplificada por meio do uso direto dos provedores, ainda que isso representasse uma limitação. As etapas e aprendizados estão detalhados em "doc_terraform_create" e "doc_terraform_teste_locais".

### 4.3 Estrutura Geral com Terraform
A estrutura adotada dividiu-se entre módulos e ambientes (environments), permitindo a reutilização da configuração para diferentes cenários (dev, hml, prod). Faltaram algumas implementações que gostaria de ter explorado, como IAM e armazenamento via Terraform.

## 5. Deploy com ArgoCD e CI/CD
Inicialmente, é necessário configurar o Terraform e aplicá-lo para criar o cluster. Com o `kubeconfig` em mãos, segue-se para o deploy dos artefatos usando o ArgoCD. Dentro do diretório `argocd-init`, o comando `argocd-install.bat` prepara o ambiente, instala o ArgoCD, o Istio e carrega as configurações de aplicação e monitoramento.

A atualização de versões da aplicação se dá através do commit no GitLab, gerando uma nova tag que é automaticamente sincronizada pelo ArgoCD, refletindo a nova versão no cluster.

## 6. Monitoramento
O monitoramento é planejado para utilizar Grafana e Prometheus, dentro da stack kube-prometheus, que já traz configurações pré-definidas e dashboards prontas. A inclusão de Loki para gerenciamento de logs, junto com Promtail e MinIO, ficará para uma fase futura.

## Conclusão
Este projeto representou um desafio significativo, saindo da minha zona de conforto, especialmente pela adoção do Terraform e pela complexidade de gerenciar uma infraestrutura Kubernetes multiambiente. Vi os benefícios dessa abordagem e reconheço os pontos que preciso melhorar. Estou confiante de que, com mais alguns meses de estudo e prática, conseguirei implementar uma solução ainda mais completa e automatizada.

Com mais tempo, identifico alguns pontos para aprimoramento. A transição do Terraform para uma estrutura baseada em módulos, removendo uso direto de providers, é um avanço que promete tornar a infraestrutura mais organizada e reutilizável. Além disso,  melhorias  nas etapas de autotag e nos stages da pipeline, garantindo que o gerenciamento de versões seja mais eficaz e alinhado com as práticas recomendadas de CI/CD. Outro aspecto importante é a implementação de um dashboard específico para logs, o uso do cert-manager para a gestão de certificados e a ativação do TLS, melhoria no diagrama apresentado.
