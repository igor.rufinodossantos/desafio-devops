# Documentação da Contrução do Dockerfile

## Introdução

Este documento consiste em explicar o processo de criação do `Dockerfile` para a aplicação e as decisões tomadas durante a criação do container Docker.

## Escolha da Imagem Base

O `Dockerfile` com a imagem base `python:3.7-slim`. Motivos da escolha da imagem:

- **`Compatibilidade`**: O arquivo `.python-version` no repositório da aplicação especifica a versão `3.7.4`. A imagem `python:3.7-slim` é compatível com essa versão, mesmo que possa ser uma versão de patch diferente, ainda está dentro da série `3.7` que é requerida pelo projeto.
- **`Tamanho`**: A opção por `slim` é pelo fato de ser uma versão reduzida da imagem completa do Python, contendo as ferramentas e bibliotecas necessárias para executar a maioria das aplicações Python. Isso mantém o tamanho do container final menor.

## Configuração do Ambiente de Trabalho

O comando `WORKDIR /api` estabelece `/api` como diretório de trabalho . Com isso temos:

- **`Organização`**: Fornece um local conhecido no container onde o código da aplicação reside.
- **`Contexto de Execução`**: Todos os comandos subsequentes no `Dockerfile` são executados neste diretório, evitando a necessidade de usar caminhos completos.

## Instalação de Dependências

Para instalar as dependências da aplicação, usei o arquivo já criado `requirements.txt`, que é copiado para dentro do container junto com o restante do código da aplicação. O comando `RUN pip install -r requirements.txt` é utilizado para:

- **`Automatização`**: Permite a instalação automática de todas as dependências listadas no arquivo, garantindo que não haja omissões.
- **`Manutenção`**: Facilita a atualização de dependências, pois basta modificar o arquivo `requirements.txt` sem a necessidade de ajustar o `Dockerfile`.

## Definição do Comando de Inicialização

A linha `CMD ["gunicorn", "-b", "0.0.0.0:8000", "api:app"]` define o comando que será executado por padrão quando o container iniciar. Esse comando utiliza o `gunicorn`, um servidor WSGI para Python que é adequado para produção, devido a:

- **`Performance`**: É capaz de lidar com mais tráfego de maneira mais eficiente que o servidor de desenvolvimento padrão do Flask.
- **`Confiabilidade`**: Tem melhor tolerância a falhas e está melhor preparado para operar em ambientes de produção.
- **`Escalabilidade`**: Facilita a execução de múltiplas instâncias para escalar a aplicação horizontalmente, se necessário.

## Conclusão

O `Dockerfile` foi  construído para criar um ambiente Docker otimizado, mantendo a consistência com os requisitos da aplicação para a implantação.

