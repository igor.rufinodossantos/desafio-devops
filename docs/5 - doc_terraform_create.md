# Documentação de Criação de VPC com Terraform para GCP

## Introdução

Este documento consiste em explicar o processo de configuração e implantação de uma VPC na GCP utilizando Terraform, para a aplicação e as decisões tomadas para a implantação de clusters Kubernetes.


## Estrutura de Diretórios e Arquivos


A estrutura é organizada em dois diretórios principais, `modules` para o código reutilizável do Terraform e `environments/develop` para as configurações específicas do ambiente de desenvolvimento:

#### Diretório `modules`

- **`locals.tf`**: Utilizado para definir valores locais que simplificam as configurações repetitivas dentro do módulo, melhorando a clareza e a reutilização do código.
- **`main.tf`**: Arquivo central do módulo que contém as definições dos recursos do GCP a serem criados, como VPC, sub-redes, e regras de firewall.
- **`outputs.tf`**: Determina quais informações dos recursos criados serão expostas para outros módulos ou configurações, facilitando a integração e o acesso a dados importantes.
- **`variables.tf`**: Declara variáveis que permitem a personalização do módulo, como nomes de VPC, configurações de sub-rede, entre outros.
- **`version.tf`**: Especifica a versão mínima do Terraform necessária e configura os provedores requeridos, garantindo compatibilidade e funcionalidade do módulo.

#### Diretório `environments/develop`

- **`credentials.json`**: Contém as credenciais da conta de serviço do GCP necessárias para autenticar e gerenciar recursos do GCP.
- **`main.tf`**: Arquivo Terraform principal para o ambiente de desenvolvimento, que usa os módulos definidos e configura recursos específicos deste ambiente.
- **`terraform.tfvars`**: Arquivo que fornece valores específicos para as variáveis definidas no ambiente de desenvolvimento e nos módulos utilizados.
- **`variables.tf`**: Declara as variáveis específicas do ambiente de desenvolvimento, possibilitando a customização de acordo com as necessidades do projeto.
- **`version.tf`**: Define a versão do Terraform e dos provedores necessários para o projeto, além de configurar o backend no GCS para o estado do Terraform, promovendo um controle eficaz e centralizado.


#### Diagrama de Estrutura de Diretórios e Arquivos com Comentários

```
environments/
└── develop/
    ├── credentials.json
    ├── main.tf
    ├── terraform.tfvars
    ├── variables.tf
    └── version.tf

modules/
 ├── locals.tf
 ├── main.tf
 ├── outputs.tf
 ├── variables.tf
 └── version.tf
 
```

## Versão do Terraform e Seleção de Provedores


### Requisitos de Versão

A especificação `required_version = ">= 0.13.0"` garante a utilização de recursos do Terraform que foram estabilizados na versão 0.13. A escolha dessa versão foi utilizada com base na documentação requrida dos providers.

### Provedores Google e Google-Beta

A definição dos provedores `google` e `google-beta` com versãos específicas (`">= 3.33, < 6"` e `">= 4.64, < 6"`) é baseada na compatibilidade com APIs do GCP e na disponibilidade de recursos necessários para a configuração da VPC e CLuster. A versão `beta` é importante para acessar funcionalidades que ainda não estão disponíveis na versão estável, enquanto a limitação superior `< 6` é responsavel por prevenir inclusões de mudanças incompatíveis.

## Configuração da VPC

### Módulo de Rede

O uso do módulo `terraform-google-modules/network/google` na versão `~> 9.0` é uma que permite aproveitar os padrões atualizados para criação de redes no GCP. Esta versão do módulo suporta configurações de criação de sub-redes, configurações de roteamento e regras de firewall dentro da VPC. A seleção de uma versão específica garante consistência e prevenção contra alterações inesperadas em atualizações futuras.

### Estratégia de Sub-rede e Roteamento

Os valores adotados seguem uma configuração padrão, fornecida pela documentação, configuração padronizada para uma sub-rede.


## Regras de Firewall

### Módulo de Regras de Firewall

A implementação do módulo `firewall-rules` segue a lógica de definição precisa do controle de tráfego. A dependência direta deste módulo com a criação da VPC (`depends_on = [module.vpc]`) assegura a ordem correta de criação dos recursos, evitando problemas de dependência 

## Outputs

A definição de `outputs` para `vpc_name` e `subnet_names`  foi importante para  integração com o setup de clusters Kubernetes. A exposição desses valores como outputs torna possível, por exemplo, a configuração dinâmica de firewalls ou políticas de roteamento baseadas nas sub-redes criadas.

## Variáveis e Locals

A configuração de `variables` e `locals` fornece facilidade na reutilização do código. 

## Conclusão

 Este documento serve como um recurso técnico completo para guiar a criação, gestão e entendimento de redes virtuais no GCP com Terraform.