# Terraform


1 - PS C:terraform\enviroments\develop> ```terraform init```



Initializing the backend...

Successfully configured the backend "gcs"! Terraform will automatically
use this backend unless the backend configuration changes.
Initializing modules...
- vpc_and_cluster in ..\..\modules
Downloading registry.terraform.io/terraform-google-modules/network/google 9.0.0 for vpc_and_cluster.firewall_rules...
- vpc_and_cluster.firewall_rules in .terraform\modules\vpc_and_cluster.firewall_rules\modules\firewall-rules
Downloading registry.terraform.io/terraform-google-modules/network/google 9.0.0 for vpc_and_cluster.vpc...
- vpc_and_cluster.vpc in .terraform\modules\vpc_and_cluster.vpc
- vpc_and_cluster.vpc.firewall_rules in .terraform\modules\vpc_and_cluster.vpc\modules\firewall-rules
- vpc_and_cluster.vpc.routes in .terraform\modules\vpc_and_cluster.vpc\modules\routes
- vpc_and_cluster.vpc.subnets in .terraform\modules\vpc_and_cluster.vpc\modules\subnets
- vpc_and_cluster.vpc.vpc in .terraform\modules\vpc_and_cluster.vpc\modules\vpc

Initializing provider plugins...
- Finding hashicorp/google-beta versions matching ">= 4.64.0, < 6.0.0"...
- Finding hashicorp/google versions matching ">= 3.33.0, >= 3.83.0, >= 4.25.0, >= 4.64.0, < 6.0.0"...
- Installing hashicorp/google-beta v5.23.0...
- Installed hashicorp/google-beta v5.23.0 (signed by HashiCorp)
- Installing hashicorp/google v5.23.0...
- Installed hashicorp/google v5.23.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository



3 - terraform validate
PS C:terraform\enviroments\develop> ```terraform validate```

Success! The configuration is valid.



4 -   terraform plan

PS C:terraform\enviroments\develop> ```terraform plan```

    Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
    + create

    Terraform will perform the following actions:

    # module.vpc_and_cluster.google_container_cluster.primary will be created

    Plan: 12 to add, 0 to change, 0 to destroy.  


5 -   terraform apply

PS C:terraform\enviroments\develop> ```terraform apply```

Apply complete! Resources: 12 added, 0 changed, 0 destroyed.


# Argocd

Pegar credentials do cluster gcp

Utilizando a cli da gcp e com as permissões de acesso ao cluster executar o comando de conexão com o cluster

    gcloud container clusters get-credentials name_cluster --zone us-central1-c --project project_id

    Fetching cluster endpoint and auth data.
    kubeconfig entry generated for desafio-teste.

Como Resultado terá o config gerado no ./kube


```Para visualizar o cluster eu uitlizo o Lens, mas pode ser utilziado o K9s, ou apenas o kubectl.```



1 - C:deployment_configs\argo-init>: kubectl config get-contexts 

CURRENT   NAME                                       CLUSTER                                    AUTHINFO                                   NAMESPACE
*         gke_still-kit-411420_us-central1-c_teste   gke_still-kit-411420_us-central1-c_teste   gke_still-kit-411420_us-central1-c_teste


2 - deployment-config\argocd-init> ```.\install_argo.bat```


    Renomeando os arquivos na pasta argocd-init...


    Adicionando o repositorio do chart do Argo CD...
    "argo" already exists with the same configuration, skipping

    Atualizando a lista de charts dispon├¡veis...
    Hang tight while we grab the latest from your chart repositories...
    ...Successfully got an update from the "prometheus-community" chart repository
    ...Successfully got an update from the "argo" chart repository
    ...Successfully got an update from the "bitnami" chart repository
    Update Complete. ⎈Happy Helming!⎈

    Instalando o chart do Argo CD...
    NAME: argocd
    LAST DEPLOYED: Sun Apr  7 16:03:07 2024
    NAMESPACE: argocd
    STATUS: deployed
    REVISION: 1
    TEST SUITE: None
    NOTES:
    In order to access the server UI you have the following options:

    1. kubectl port-forward service/argocd-server -n argocd 8080:443

        and then open the browser on http://localhost:8080 and accept the certificate

    2. enable ingress in the values file `server.ingress.enabled` and either
        - Add the annotation for ssl passthrough: https://argo-cd.readthedocs.io/en/stable/operator-manual/ingress/#option-1-ssl-passthrough
        - Set the `configs.params."server.insecure"` in the values file and terminate SSL at your ingress: https://argo-cd.readthedocs.io/en/stable/operator-manual/ingress/#option-2-multiple-ingress-objects-and-hosts


    After reaching the UI the first time you can login with username: admin and the random password generated during the installation. You can find the password by running:

    kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d

    (You should delete the initial secret afterwards as suggested by the Getting Started Guide: https://argo-cd.readthedocs.io/en/stable/getting_started/#4-login-using-the-cli)

    Pausando o script por 20 segundos...

    Aguardando 10 segundos, pressione uma tecla para continuar ...

    Configurando o Istio...
    Adicionando o comando istioctl ao PATH...

    Instalando o Istio default...
    ✔ Istio core installed
    ✔ Istiod installed
    ✔ Ingress gateways installed
    ✔ Installation complete                                                                                                                                                                                                                                  Making this installation the default for injection and validation.

    Thank you for installing Istio 1.17.  Please take a few minutes to tell us about your install/upgrade experience!  https://forms.gle/hMHGiwZHPU7UQRWe9

    Pausando o script por 10 segundos...

    Aguardando  0 segundos, pressione uma tecla para continuar ...

    Aplicando a configura├º├úo de Application do Argo CD...
    application.argoproj.io/init created

    Renomeando os arquivos na pasta argocd-init...


    Script conclu├¡do com sucesso!


    