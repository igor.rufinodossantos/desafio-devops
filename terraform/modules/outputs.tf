output "vpc_name" {
  value = module.vpc.network_name
}


output "subnet_names" {
  value = [for subnet in var.subnets : subnet.subnet_name]
}

