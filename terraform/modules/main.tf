# VPC
module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "~> 9.0"


  project_id   = var.project_id
  network_name = var.vpc_name
  routing_mode = var.routing_mode
  subnets      = var.subnets
  routes       = var.routes
}

# FIREWALL

module "firewall_rules" {
  source       = "terraform-google-modules/network/google//modules/firewall-rules"
  project_id   = var.project_id
  network_name = module.vpc.network_name

  rules      = local.firewall_rules
  depends_on = [module.vpc]
}

# CLUSTER

resource "google_container_cluster" "primary" {
  name                     = var.cluster_name
  location                 = var.group_zones
  remove_default_node_pool = var.remove_default_node_pool
  initial_node_count       = var.node_size
  deletion_protection      = var.deletion_protection
  private_cluster_config {
    enable_private_endpoint = var.enable_private_endpoint
    enable_private_nodes    = var.enable_private_nodes
    master_ipv4_cidr_block  = var.master_ipv4_cidr_block
  }


  network            = var.vpc_name
  subnetwork         = var.selected_subnet_name
  logging_service    = "none"
  monitoring_service = "none"

  depends_on = [module.vpc]
}


resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = var.name_pool
  cluster    = var.cluster_name
  location   = var.group_zones
  node_count = var.node_size

  node_config {
    preemptible  = true
    machine_type = var.instance_type
    oauth_scopes = ["https://www.googleapis.com/auth/cloud-platform"]
    disk_size_gb = var.disk_size
    disk_type    = var.disk_type
  }

  depends_on = [google_container_cluster.primary]
}
