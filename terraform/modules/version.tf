terraform {
  required_version = ">= 0.13.0"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.33, < 6"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 4.64, < 6"
    }
  }

}

provider "google" {
  credentials = file("./credentials.json")
  project     = var.project_id
  region      = var.group_regions
}

provider "google-beta" {
  credentials = file("./credentials.json")
  project     = var.project_id
  region      = var.group_regions
}