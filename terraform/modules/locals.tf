locals {
  firewall_rules = concat(
    [
      for rule in var.simple_firewall_rules : {
        name               = "${rule.name}-ingress"
        description        = "Firewall rule for ${rule.name} (ingress)"
        direction          = "INGRESS"
        priority           = 1000
        source_ranges      = ["0.0.0.0/0"]
        destination_ranges = []
        allow              = [{ protocol = "tcp", ports = rule.ports }],
        deny               = [],
        log_config         = { metadata = "INCLUDE_ALL_METADATA" }
      }
    ],
    [
      for rule in var.simple_firewall_rules : {
        name               = "${rule.name}-egress"
        description        = "Firewall rule for ${rule.name} (egress)"
        direction          = "EGRESS"
        priority           = 1000
        source_ranges      = []
        destination_ranges = ["0.0.0.0/0"]
        allow              = [{ protocol = "tcp", ports = rule.ports }],
        deny               = [],
        log_config         = { metadata = "INCLUDE_ALL_METADATA" }
      }
    ],
    # Adicionando regra para permitir ICMP
    [
      {
        name               = "allow-icmp"
        description        = "Allow ICMP traffic"
        direction          = "INGRESS"
        priority           = 1000
        source_ranges      = ["0.0.0.0/0"]
        destination_ranges = []
        allow              = [{ protocol = "icmp" }]
        deny               = []
        log_config         = { metadata = "INCLUDE_ALL_METADATA" }
      }
    ]
  )
}