# PROJETO

variable "project_id" {
  type        = string
  description = "ID do projeto GCP."
}

variable "credentials" {
  description = "Credenciais da conta de serviço do GCP."
}

variable "group_regions" {
  type        = string
  description = "Regiões padrões a serem criadas as subnets"

}

# VPC

variable "vpc_name" {
  type        = string
  description = "Nome da VPC"
}
variable "routing_mode" {
  description = "Modo de roteamento da VPC ('GLOBAL' ou 'REGIONAL')."
}

# SUBNET

variable "subnets" {
  description = "Sub-redes para criar na VPC."
  type = list(object({
    subnet_name           = string
    subnet_ip             = string
    subnet_region         = string
    subnet_private_access = bool
    subnet_flow_logs      = bool
  }))
}

variable "routes" {
  description = "Sub-redes para criar na VPC."
  type = list(object({
    name              = string
    description       = string
    destination_range = string
    next_hop_internet = bool

  }))
}


variable "selected_subnet_name" {
  description = "The name of the subnet to use for the cluster."
  type        = string
}

#FIREWALL

variable "simple_firewall_rules" {
  description = "Simplified firewall rules specifying only name and ports."
  type = list(object({
    name  = string
    ports = list(string)
  }))
}

#CLUSTER

variable "cluster_name" {
  type        = string
  description = "Nome do Cluster"
}

variable "deletion_protection" {
  description = "Protege o cluster GKE contra exclusão acidental."
  type        = bool
}

variable "group_zones" {
  type        = string
  description = "Zonas onde as instancias serão provisionadas"

}

variable "remove_default_node_pool" {
  type        = bool
  description = "remoção node inicial default"

}

variable "enable_private_endpoint" {
  type        = bool
  description = "remoção node inicial default"

}

variable "enable_private_nodes" {
  type        = bool
  description = "remoção node inicial default"

}

variable "master_ipv4_cidr_block" {
  type        = string
  description = "Lista de CIDRs autorizados a acessar o mestre do GKE"

}

variable "instance_type" {
  type        = string
  description = "Tipos de instâncias"
}

/******************************************
	node 
 *****************************************/

variable "node_size" {
  type        = number
  description = ""
}

variable "disk_type" {
  type        = string
  description = "Tipos de disco"
}

variable "disk_size" {
  type        = number
  description = "Tipos de disco"

}

variable "name_pool" {
  type        = string
  description = "Nome dos pools"
}
