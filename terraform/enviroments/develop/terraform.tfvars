# Projeto
project_id    = "<name_projeto"
credentials   = "credentials.json>"
group_regions = "<name_da_regiao>"

#VPC
vpc_name     = "vpc-name"
routing_mode = "GLOBAL"

# SUBNET
subnets = [
  {
    subnet_name           = "sub-rede-name"
    subnet_ip             = "10.10.20.0/24"
    subnet_region         = "<name_da_regiao>"
    subnet_private_access = false
    subnet_flow_logs      = true
  },
  // Adicione mais subnets aqui conforme necessário
]

routes = [
  {
    name              = "default"
    description       = "route through IGW to access internet"
    destination_range = "0.0.0.0/0"
    next_hop_internet = "true"

  },
  // Adicione mais subnets aqui conforme necessário
]


# FIREWALL
simple_firewall_rules = [
  {
    name  = "allow-ssh-dev",
    ports = ["22"]
  },
  {
    name  = "allow-http-dev",
    ports = ["80"]
  },
  {
    name  = "allow-https-dev",
    ports = ["443"]
  }

]

#Cluster
cluster_name             = "cluster-name"
group_zones              = "<name_da_zona>"
deletion_protection      = false
selected_subnet_name     = "sub-rede-name"
remove_default_node_pool = true
enable_private_endpoint  = false
enable_private_nodes     = false
master_ipv4_cidr_block   = "172.16.0.0/28"
instance_type            = "e2-medium"
disk_type                = "pd-balanced"
disk_size                = 20
name_pool                = "pool"
node_size                = 2