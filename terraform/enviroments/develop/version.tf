terraform {
  required_version = ">= 0.13.0"

  backend "gcs" {
    bucket      = "<bucket_name>"
    prefix      = "<caminho_internto_backut>"
    credentials = "credentials.json"
  }

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.33, < 6"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 4.64, < 6"
    }
  }

}

provider "google" {
  credentials = file("./credentials.json")
  project     = var.project_id
  region      = var.group_regions
  zone        = var.group_zones
}

provider "google-beta" {
  credentials = file("./credentials.json")
  project     = var.project_id
  region      = var.group_regions
  zone        = var.group_zones
}