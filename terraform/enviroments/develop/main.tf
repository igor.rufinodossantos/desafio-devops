module "vpc_and_cluster" {
  source = "../../modules" // Certifique-se de que o caminho para o módulo esteja correto

  project_id               = var.project_id
  vpc_name                 = var.vpc_name
  subnets                  = var.subnets
  routes                   = var.routes
  routing_mode             = var.routing_mode
  simple_firewall_rules    = var.simple_firewall_rules
  credentials              = var.credentials
  group_regions            = var.group_regions
  cluster_name             = var.cluster_name
  group_zones              = var.group_zones
  remove_default_node_pool = var.remove_default_node_pool
  node_size                = var.node_size
  selected_subnet_name     = var.selected_subnet_name
  deletion_protection      = var.deletion_protection
  enable_private_endpoint  = var.enable_private_endpoint
  enable_private_nodes     = var.enable_private_nodes
  master_ipv4_cidr_block   = var.master_ipv4_cidr_block
  instance_type            = var.instance_type
  disk_size                = var.disk_size
  disk_type                = var.disk_type
  name_pool                = var.name_pool

}
