#!/bin/bash

# Recebe a nova tag como argumento
NEW_TAG=$1

# Caminho para o arquivo comentarios.yaml no repositório
COMMENTS_PATH="deployment_configs/apis/comentarios.yaml"

# Atualiza a tag da imagem no arquivo comentarios.yaml
sed -i "s/tag: .*/tag: \"$NEW_TAG\"/" $COMMENTS_PATH

# Verifica se a atualização foi bem-sucedida e sai se não
if ! grep -q "tag: \"$NEW_TAG\"" $COMMENTS_PATH; then
    echo "A atualização da tag falhou."
    exit 1
fi

echo "Tag da imagem atualizada com sucesso."
