#!/bin/bash

# Caminho para o arquivo version.json
VERSION_FILE="version.json"

# Seleciona a chave e a lógica de incremento baseada na branch
if [ "$CI_COMMIT_REF_NAME" == "develop" ]; then
    # Para 'develop', incrementamos a versão 'alpha'
    VERSION_KEY="alpha"
    NEW_VERSION=$(jq -r ".alpha" $VERSION_FILE | awk -F. -v OFS=. '{$4 = $4 + 1; print}')
elif [ "$CI_COMMIT_REF_NAME" == "main" ]; then
    # Para 'main', podemos querer atualizar 'master' ou outra chave específica
    VERSION_KEY="main" # Atualize isso conforme necessário
    NEW_VERSION=$(jq -r ".main" $VERSION_FILE | awk -F. -v OFS=. '{$3 = $3 + 1; $4="0"; print}')
    # Adapte a lógica de incremento conforme sua estratégia de versionamento para lançamentos estáveis
fi

# Atualiza version.json com a nova versão
jq ".$VERSION_KEY = \"$NEW_VERSION\"" $VERSION_FILE > temp.$$.json && mv temp.$$.json $VERSION_FILE

# Exporta a nova tag para uso no GitLab CI/CD
echo "export NEW_TAG='$NEW_VERSION'" > $CI_PROJECT_DIR/version_env.sh